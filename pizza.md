# Pizza

Portionen: 1 Blech
Dauer:

* 30 Minuten bis 24 Stunden gehen (siehe unten)
* 20 Minuten Ausrollen und belegen
* ~7 +Backen

## Zutaten

### Teig

* 400g Weizenmehl Typ 405 (400g ergeben ein dickes Blech)
* 200ml Wasser
* Hefe (Auf der Packung steht in der Regel für wie viel Mehl die Hefe geeignet ist.)
* 4g Salz
* ~10g Zucker

### Belag

* Passierte Tomaten
* Knoblauch
* Geriebener Käse (Besser Gouda, als „Pizzakäse‟, wer weiß was da drin ist)
* Oregano oder Majoran
* Was man sonst noch mag
* Frischer Basilikum zum garnieren

## Zubereitung

### Temperatur

Troken-Hefe sollte in Wasser (Zimmer-Temperatur) aufgelöst werden.

Das Problem ist, dass diese sich in einer Maschine nicht so leicht kneten lassen.
Der Teig klumpt und klebt ohne sich kneten lassen zu wollen.
Am Besten lassen sich die Zutaten in einer Küchenmaschine kneten, wenn Mehl und Wasser vor dem anstellen komplett in die Maschine gegeben wurde.
Das das Wasser heiß ist, hilft dem Teig beim gehen, besonders wenn man etwas weniger Zeit hat.
Soll der Teig eh 12 Stunden gehen, kann es auch kalt sein.

### Gehen

Der Teig sollte 4 Stunden gehen.
Unter guten Bedingungen reicht das.
Richtig gut ist er auf jeden Fall nach 12 Stunden, es macht ihm aber auch nichts aus, wenn der Teig 24 Stunden geht. Dabei sollte er keinen Zug bekommen und nicht austrocknen. Man kann den Teig auch nach 30 Minuten verwenden, dann ist er halt nicht so gut.
Ausrollen
Ausrollen geht leicht, wenn man den Teig häufig mit Mehl bestreicht und wendet. Also: Mit Mehl bestreichen, ausrollen, mit Mehl bestreichen, wenden, mit Mehl bestreichen, ausrollen, u.s.w.
Belgen
Der Belag ist natürlich beliebig, aber die Reihenfolgen ist natürlich immer:
Ganz Oben: Oregano
Oben: Käse
Mitte: Belag (nach belieben, z.B. Knoblauch, Jalapeños, etc.)
Unten: Tomaten
Backen
250 °C oder auch etwas mehr. Umluft hilft. Der Backofen sollte komplett vorgeheizt sein. Nur ein Blech pro Backvorgang in den Ofen schieben. Das ist sehr wichtig, da die Pizza sonst nicht knusprig wird. Das hängt aber natürlich auch vom Backofen ab.

7 Minuten Backzeit genügen.

Mit frischem Basilikum garnieren.
Pizzabrötchen
Aus dem übrigen Teig können Pizzabrötchen gemacht werden. Einfach den Teig in Rollenform bringen, alle 3 cm abschneiden und backen.
