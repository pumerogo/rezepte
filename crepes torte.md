# Crêpes

300g Mehl
750ml Milch
75g Mohn
6 Eier
2 Päckchen Vanillezucker
75g flüssige Butter
2 Prisen Salz

Mixen, in Fett zu Crêpes backen

# Füllung

320g Lemon Curd minus 2EL Lemon Curd
500g Quark
4EL Zucker
500ml Sahne
2 Päckchen Sahnestei

# Garnitur

1 Vanilleschote
2EL von 320g Lemon Curd
