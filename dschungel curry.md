# Dschungel Curry

[Quelle](https://www.reddit.com/r/veganrecipes/comments/gop0im/jungle_curry_nb_very_not_for_the_faint_hearted/)

## Currypaste

* 10-15 Bird's eye chilis ???
* 1Pr Salz
* 1EL Ingwer, gehackt (galangal⁣???)
* 2TL Zitronengras, gehackt
* 1EL limettenschale
* 2TL Korreanderstengel, gehackt
* 3EL Schalotten, gehackt
* 2EL Knoblauch, gehackt

### Optional

* 1TL Garnelenpasten-Ersatz der Wahl: weiße Miso, gelbe bohnen soße, tua naow ???

Alle Zutaten pürieren.

## Knoblauchpaste

* 2 Knoblauchzehen, chopped⁣
* 1Pr Salz
* 3 Bird's eye chilis ???

Alle Zutaten pürieren.

## Curry

* 200g fritiertes Tofu (also vorfritiertes tofu???)
* 240ml Gemüsebrühe
* 1EL Soya Soße
* 1/2TL Zucker
* 2EL Korreander, gehackt

### Optional

* 1EL firtierte Schalotten

⁣Topf auf mittlere hitze bringen.
1EL Öl hinzufügen.
Stir fry ??? Knoblauchpaste für ca. 1 Minuten.
Currypaste hinzufügen.
Stir fry ??? für ca. 3 Minuten.
Zucker, Sojasoße und Brühe hinzufügen.
Aufkochen lassen.
Tofu hinzufügen.
2 Minuten köcheln lassen.

Mit Korreander und firtierten Schalotten garnieren.

Mit Reis servieren.
