# Ramen

[Quelle](https://www.reddit.com/r/VeganDE/comments/gohbg2/veganer_ramen/)

* Karotten
* getrockneten Shiitake-Pilzen
* kleinen Stückchen Nori-Blättern

anbraten.

Mit Reisessig ablöschen

* Wasser 
* Sojasoße

circa eine Stunde köcheln lassen

Teesieb mit was zum Mitköcheln rein (Petersilie, Knoblauch)

Einlagen:

* Austernpilze
* Tofu angebraten
* Lauchzwiebel
* Spinat
* Sesamsamen
* Süßkartoffeln
* Zuckererbsen
